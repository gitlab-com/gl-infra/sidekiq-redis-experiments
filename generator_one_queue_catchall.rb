require 'sidekiq'
require 'active_support'
require_relative 'application_worker.rb'

puts "Starting sidekiq load generation"

Dir.glob("workers/**/*.rb").each {|file| require_relative file}

# Simulate making catchall-k8s (largest number of queues) use only the 'default' queue
# instead of a queue per worker, but leave the rest doing their usual thing 
def schedule(type, klass)
  (COUNTS[type.to_sym]*2).times do
    if (type == 'catchall-k8s')
      klass.set(queue: 'default').perform_async(1,2,3)
    else
      klass.perform_async(1,2,3)
    end
  end
end

#Number of workers for each shard, so we can scale our generation appropriately
COUNTS = {
  'catchall-k8s': 196*15,
  'catchall-vm': 56*15,
  'memory-bound': 16*1,
  'database-throttled': 1*5,
  'gitaly-throttled': 3*8,
  'elasticsearch': 8*2,
  'low-urgency-cpu-bound': 100*5,
  'urgent-cpu-bound': 84*5,
  'urgent-other': 130*5
}
count = 0
loop do
  puts "Round #{count} at #{Time.now.strftime('%H:%M:%S.%L')}" 

  schedule('catchall-k8s', VulnerabilityExportsExportDeletionWorker)
  schedule('catchall-vm', GroupsUpdateStatisticsWorker)
  schedule('database-throttled', BackgroundMigrationWorker)
  schedule('elasticsearch', ElasticIndexerWorker)
  schedule('gitaly-throttled', SnippetsUpdateRepositoryStorageWorker)
  schedule('low-urgency-cpu-bound', NewEpicWorker)
  schedule('memory-bound', ProjectExportWorker)
  schedule('urgent-cpu-bound', MergeRequestResetApprovalsWorker)
  schedule('urgent-other',UpdateHighestRoleWorker)

#  sleep(2)
  count = count + 1
end
