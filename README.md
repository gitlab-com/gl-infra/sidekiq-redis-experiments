A test harness with which we can run Sidekiq scalability experiments, particularly targeting the impact of those on Redis.

See https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/956 for the instigation, links to related work, and some experiements (and their results) already conducted using this.

# Structure/Basis

The code/config is a bit of a mess at this point, most is in the top level directory. We've worked on it incrementally and there's definitely opportunities to make it tidier. Feel free if you have a moment, or just iterate on it when working on further experiments.

In terms of servers, we expect 2 or 3 nodes:
* A redis server
* A client server (running sidekiq client and workers)
* An optional prometheus

On the client, we use the [REDIS_URL environment variable](https://www.rubydoc.info/gems/redis#getting-started) (e.g. `redis://1.2.3.4:6379`) to configure the address and password of the Redis server.

The basic idea is to run a set of sidekiq workers configured similarly to production in size/shape, executing workloads which do nothing but sleep for a suitable amount of time (randomly chosen from a distribution that looks a lot like production).  Then we shove work into the sidekiq queues in proportions that look a lot like production.  This can all happen from one client node and drives load on Redis that fairly closely resembles what we see on production.

We can then vary configuration in specific ways to see what will happen on Redis.  This can be done a lot quicker (minutes to hours) than it would take to do the same in the GitLab code base, taking into account all the complications that entails.  As a result, we can make predictions about what approach will have the best effect, what negative effects might be observed, and so on.

## Specifics
* `*.yml`: Sidekiq worker configurations
* `*.txt`: Data that defines the workers for each shard and what proportions to execute them in
   * Derived from raw log data (see https://log.gprd.gitlab.net/goto/6e896049ab0d2bdd448b1f8205f91ebb); can be updated if you want
   * Is a list of queues/workers (one per line) with an absolute number (from the logs).  The absolute numbers are summed at runtime to derive proportions.
   * There *must* be a worker in the workers directory for each named queue
* `application_worker.rb`: A concern/mixin that helps emulate current GitLab sidekiq worker behavior regarding queue names, and some other bits to make simulations more plausible/accurate.  Included in every worker.
* `check_queues.rb`: A script for inspecting the queue state (size of queues).  Run with `ruby check_queues.rb`
* `create_worker_classes.rb`: A script to create the worker classes that go in the `workers` directory
   * Not run from here; run it from a clone of the gitlab code base.  It looks for all queues/workers currently configured in the app and creates a boilerplate worker class for each in `/tmp/workers`.  Note that the mapping is not the same as we have in GitLab (where the worker class name might not directly match the queue name, particularly with namespaces, where the namespace path of the queue doesn't have to match the path of the class); for simplicity, we take the *queue* name, and turn that into a module + class path to match.  It's close enough for practical purposes.
   * Run with `bundle exec ruby create_worker_classes.rb`.  You only need to do this if the distribution of workers/jobs has changed significantly since the last time they were updated, and we need more realistic data.
   * After running, copy `/tmp/workers` into the `workers` directory in this repo, and then update the .txt files to match current distributions of workers (from logs).  Or add new entries for new workers, at whatever proportions you like, if you're confident you are estimating well.
* `generator*`: Code to generate jobs into Sidekiq queues; generally one per experiment.  You're probably going to need to copy an existing one and adjust it for the experiment you want to conduct.
* `one_queue_per_shard`: A directory with sidekiq config yml files suitable for running a "one queue per shard" experiment
* `run_experiment.rb`: Runs the entire experiment, configured for the most recent one (sampled brpop queues: https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/13024).  You'll likely need to copy/edit this for your specific experiments.
u `run-multiple-sidekiqs-one-queue-per-shard.sh` and `run-multiple-sidekiqs.sh`: Simple scripts to run  many workers for a given shard type (used by `run_experiment.rb` or by hand)
* `sidekiq.rb`: Used by `run-multiple-sidekiq*.sh` to properly configure sidekiq for the experiments, and output logs for each to the logs directory.  You probably won't have to tinker with this.

# Setup

## Redis

Setting up a Redis server to use for this is a bit manual, but not much work.

If you are not comparing different Redis versions, just install omnibus-gitlab on a VM and configure it as a standalone Redis server:

```ruby
# /etc/gitlab/gitlab.rb

## Enable Redis and disable all other services
## https://docs.gitlab.com/omnibus/roles/
roles ['redis_master_role', 'monitoring_role']

## Redis configuration
redis['bind'] = '0.0.0.0'
redis['port'] = 6379
#redis['password'] = 'abcdefgh'

# Use ssh -L localhost:9090:localhost:9090 to make the Prometheus instance on the Redis server browsable on your local computer.
redis_exporter['flags'] = {
  'redis.addr' => 'localhost:6379',
  'redis.password' => redis['password'],
  'check-keys' => "'queue:*'", # observe queue sizes via redis_key_size metric
}

# Lower scrape intervals are useful here to catch transient spikes
prometheus['scrape_interval'] = 1
prometheus['scrape_timeout'] = 1

# process-exporter
prometheus['scrape_configs'] = [
  {
    'job_name': 'process-exporter',
    'metrics_path': '/metrics',
    'static_configs' => [
      'targets' => ['localhost:9256'],
    ],
  },
]

## Disable automatic database migrations
## Only the primary GitLab application server should handle migrations
gitlab_rails['auto_migrate'] = false
```

If you want to compare different Redis versions, follow the instructions below.

1. Create a Ubuntu VM; ensure it is equivalently spec'd to whatever VMs are in the production environment you're trying to compare to.  Most important here is the type/model/variant of CPU, because even with recent threading additions in Redis, the core of the important work on the data store is completely single threaded.  Actual core count matters less (other than more than 2 and probably 4+ to ensure Redis gets clear access to one full CPU at all times; also typically with Sidekiq the amount of memory doesn't really matter with any multi-core machine in e.g. GCP. A few GBs is fine, we won't likely be stressing that out.
  * For initial tests, this was a c2-standard-8 (8CPUs, 32GB RAM).
  * Ensure only ssh is open to the world; the Redis config (see below) is trivial and not safe for exposure to the internet.
1. Create a directory /opt/redis
1. Copy the config files from this repo, in the redis directory, to /opt/redis
   * These are derived from GitLab production infra, with a few minor changes (including a trivial hardcoded password).  
1. Obtain binaries for redis and redis-cli to match the versions you want to test.  Put them in /opt/redis, named "redis-server-$VERSIOn" and "redis-cli-$VERSION".  You should be able to get these from GitLab deployments (embedded in the omnibus packages) or wherever else you want.  
1. Copy redis/select-redis.sh from this repo to the Redis server (/usr/local/bin is a good location).
  * This is cheap'n'cheerful, and currently refers to known versions used in experiments to date.  Update/tweak it as necessary, it's nothing special

### Running
1. `/usr/local/bin/select-redis.sh <VERSION>`
   * where VERSION is, at this writing, one of 5, 6, or 6p.  This selects 5.0.9, 6.0.10, or 6.0.10 with a BRPOP regression patch.
1. sudo redis-server

When done, ctrl-c, select another version for further experiments, and 

### Adding more varieties of Redis

Find some binaries (redis-server and redis-cli), pop them into /opt/redis, update select-redis.sh to let you select the version, and copy/create a new config file for that version (typically just need to change the `dir` configuration item, unless newer versions have other mandatory additions/changes) 

## Prometheus

I recommend using node_exporter (default config), the [redis_exporter](https://github.com/oliver006/redis_exporter)  (default config) and [process_exporter](https://github.com/ncabatoff/process-exporter) run as:

```
/opt/process-exporter/process-exporter --config.path /opt/process-exporter/config.yaml --web.listen-address=:9256 -threads=true -gather-smaps=false
```

where config.yaml is in this repo, in process-exporter.  This is consistent with GitLab production and lets you see (with Redis 6) per-thread usage.

## Client

This is where most of the work is done.  It is just a standard Ubuntu VM; in experiments to date a c2-standard-16 (16 CPU + 64GB RAM) was more than sufficient if not overkill for the running phase of experiments (although startup was noisy and might be worth the extra).  Given that typically we're trying to exercise Redis, bigger is mostly better at this end, so we can run the experiment from one location rather than multiple clients. 

Install omnibus-gitlab on the client VM to get the same Ruby runtime the application uses. It is not necessary to run `gitlab-ctl reconfigure`; you only need to install the package with `apt-get` to get a working `ruby`.

Add /opt/gitlab/embedded/bin to your path so you can just run "ruby" and have the right thing happen

Copy this repo to the VM; this is where you'll run experiments from.

# Running experiments

This assumes you have a redis server configured, and have exported [REDIS_URL](https://www.rubydoc.info/gems/redis#getting-started) in your shell to point to it.

Note that run_experiment.rb does much of this for  you, but for only one experimental variant.  Either update it to your needs, or run by hand as you see fit.

## Start the workers
Use `./run-multiple-sidekiqs.sh <SHARD> <COUNT>`, one for each shard you want to run.  Suitable gitlab.com production quantities are encoded in run_experiment.rb and recorded in https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/956.  The shard name must match a .yml file in the top level of this repo, that is configured with a list of queues to listen to and a concurrency.  If you're experimenting with other sharding patterns, you'll need to create/update those to match your desired architecture.

Note that `run-multiple-sidekiqs-one-queue-per-shard.sh` does exactly the same thing, but uses configs in `one_queue_per_shard/` instead.  If you're going to spend time here, consider creating a directory per worker configuration pattern, move the top level yml files into it, and updat ethe run script to take an additional directory parameter.

Wait.  You will see load spike significantly as Ruby starts up all these sidekiq workers.  It can take up to a minute or two, depending on your CPU resources.  You can tail the logs in `logs/*` to see when they report that they're listening for jobs, or just watch for load to peak and start subsiding. 

## Start the clients

Once the workers are stable and running, start the generator of choice.  This is entirely up to you which one, and if you're running new experiements you may well have to create your own from one of the existing examples.  

Observe, using either top or prometheus or your other observability tool of choice, the load on Redis (or whatever other metrics you're trying to see).

Note that in practice, to simulate production level loads, we need to run 2 or 3 generator scripts simultaneously.  Actual results will vary with your experiment.  The generator probably needs to be a bit smarter (the `*2` repeat on worker counts is possibly quite wrong now, particularly with the worker distribution code), but we'd need to re-do baselines after any changes to that, to ensure we're getting valid comparison data.

When done, ctrl-c the generator jobs, then stop the workers as well.
