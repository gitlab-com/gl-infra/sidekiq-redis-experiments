if ARGV.size != 2
  abort "Usage: run_experiment.rb SCENARIO NUM_GENERATORS"
end

scenario = ARGV.shift
num_generators = Integer(ARGV.shift)

require 'sidekiq'
require 'active_support'
require_relative 'application_worker.rb'

Sidekiq.redis { |r| r.flushdb }

pids = []

{
  "catchall-vm" => 56,
  "catchall-k8s" => 140,
  "memory-bound" => 16,
  "database-throttled" => 1,
  "gitaly-throttled" => 3,
  "elasticsearch" => 8,
  "low-urgency-cpu-bound" => 100,
  "urgent-cpu-bound" => 84,
  "urgent-other" => 130,
}.each do |shard, count|
  pids << spawn("./run-multiple-sidekiqs.sh #{scenario} #{shard} #{count}", [:out, :err] => "/dev/null")
end

num_generators.times { pids << spawn("ruby #{scenario}/generator.rb") }

STDIN.gets

pids.each { |pid| Process.kill("INT", pid) }

sleep 10

pids.each { |pid| Process.kill("TERM", pid) }

sleep 10

pids.each { |pid| Process.kill("KILL", pid) }

pids.each { |pid| Process.wait(pid) }
