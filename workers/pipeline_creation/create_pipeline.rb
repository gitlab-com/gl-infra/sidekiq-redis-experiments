module PipelineCreation
      class CreatePipelineWorker
      include ApplicationWorker
      
      def perform(*args)
      Sidekiq.redis { |r| r.ping } # hack to see job processing rate in redis stats
      sleep = self.class.job_duration 
      puts "CreatePipeline sleeping for #{sleep}"
      sleep(sleep)
      end
      end

end