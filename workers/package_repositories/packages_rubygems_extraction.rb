module PackageRepositories
      class PackagesRubygemsExtractionWorker
      include ApplicationWorker
      
      def perform(*args)
      Sidekiq.redis { |r| r.ping } # hack to see job processing rate in redis stats
      sleep = self.class.job_duration 
      puts "PackagesRubygemsExtraction sleeping for #{sleep}"
      sleep(sleep)
      end
      end

end