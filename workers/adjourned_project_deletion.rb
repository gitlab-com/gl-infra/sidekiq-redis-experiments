      class AdjournedProjectDeletionWorker
      include ApplicationWorker
      
      def perform(*args)
      sleep = self.class.job_duration 
      puts "AdjournedProjectDeletion sleeping for #{sleep}"
      sleep(sleep)
      end
      end
