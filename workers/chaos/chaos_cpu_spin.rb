module Chaos
      class ChaosCpuSpinWorker
      include ApplicationWorker
      
      def perform(*args)
      Sidekiq.redis { |r| r.ping } # hack to see job processing rate in redis stats
      sleep = self.class.job_duration 
      puts "ChaosCpuSpin sleeping for #{sleep}"
      sleep(sleep)
      end
      end

end