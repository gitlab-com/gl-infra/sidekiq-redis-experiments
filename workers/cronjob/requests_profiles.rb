module Cronjob
      class RequestsProfilesWorker
      include ApplicationWorker
      
      def perform(*args)
      Sidekiq.redis { |r| r.ping } # hack to see job processing rate in redis stats
      sleep = self.class.job_duration 
      puts "RequestsProfiles sleeping for #{sleep}"
      sleep(sleep)
      end
      end

end