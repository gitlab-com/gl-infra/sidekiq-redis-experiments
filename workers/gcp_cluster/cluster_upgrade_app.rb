module GcpCluster
      class ClusterUpgradeAppWorker
      include ApplicationWorker
      
      def perform(*args)
      Sidekiq.redis { |r| r.ping } # hack to see job processing rate in redis stats
      sleep = self.class.job_duration 
      puts "ClusterUpgradeApp sleeping for #{sleep}"
      sleep(sleep)
      end
      end

end