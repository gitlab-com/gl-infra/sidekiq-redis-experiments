module AutoMerge
      class AutoMergeProcessWorker
      include ApplicationWorker
      
      def perform(*args)
      Sidekiq.redis { |r| r.ping } # hack to see job processing rate in redis stats
      sleep = self.class.job_duration 
      puts "AutoMergeProcess sleeping for #{sleep}"
      sleep(sleep)
      end
      end

end