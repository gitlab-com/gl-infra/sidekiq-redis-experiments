module PersonalAccessTokens
      class PersonalAccessTokensGroupsPolicyWorker
      include ApplicationWorker
      
      def perform(*args)
      Sidekiq.redis { |r| r.ping } # hack to see job processing rate in redis stats
      sleep = self.class.job_duration 
      puts "PersonalAccessTokensGroupsPolicy sleeping for #{sleep}"
      sleep(sleep)
      end
      end

end