module Geo
      class GeoReverificationBatchWorker
      include ApplicationWorker
      
      def perform(*args)
      Sidekiq.redis { |r| r.ping } # hack to see job processing rate in redis stats
      sleep = self.class.job_duration 
      puts "GeoReverificationBatch sleeping for #{sleep}"
      sleep(sleep)
      end
      end

end