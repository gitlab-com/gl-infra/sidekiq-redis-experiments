require 'sidekiq/job_logger'

# Based on https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/sidekiq_logging/structured_logger.rb
class StructuredLogger < Sidekiq::JobLogger
  class ClientMiddleware
    def call(_worker_class, job, _queue, _redis_pool)
      if job.key?('at')
        job['scheduled_at'] = job['at']
      end

      job
    end
  end

  def call(job, queue)
    started_time = get_time
    base_payload = job

    Sidekiq.logger.info log_job_start(job, base_payload)

    yield

    Sidekiq.logger.info log_job_done(job, started_time, base_payload)
  rescue Sidekiq::JobRetry::Handled => job_exception
    # Sidekiq::JobRetry::Handled is raised by the internal Sidekiq
    # processor. It is a wrapper around real exception indicating an
    # exception is already handled by the Job retrier. The real exception
    # should be unwrapped before being logged.
    #
    # For more information:
    # https://github.com/mperham/sidekiq/blob/v5.2.7/lib/sidekiq/processor.rb#L173
    Sidekiq.logger.warn log_job_done(job, started_time, base_payload, job_exception.cause || job_exception)

    raise
  rescue StandardError => job_exception
    Sidekiq.logger.warn log_job_done(job, started_time, base_payload, job_exception)

    raise
  end

  private

  def base_message(payload)
    "#{payload['class']} JID-#{payload['jid']}"
  end

  def log_job_start(job, payload)
    payload['message'] = "#{base_message(payload)}: start"
    payload['job_status'] = 'start'

    scheduling_latency_s = queue_duration_for_job(payload)
    payload['scheduling_latency_s'] = scheduling_latency_s if scheduling_latency_s

    enqueue_latency_s = enqueue_latency_for_scheduled_job(payload)
    payload['enqueue_latency_s'] = enqueue_latency_s if enqueue_latency_s

    payload
  end

  def log_job_done(job, started_time, payload, job_exception = nil)
    payload = payload.dup

    elapsed_time = elapsed(started_time)
    add_time_keys!(elapsed_time, payload)

    message = base_message(payload)

    if job_exception
      payload['message'] = "#{message}: fail: #{payload['duration_s']} sec"
      payload['job_status'] = 'fail'
      payload['error_message'] = job_exception.message
      payload['error_class'] = job_exception.class.name
    else
      payload['message'] = "#{message}: done: #{payload['duration_s']} sec"
      payload['job_status'] = 'done'
    end

    payload
  end

  def add_time_keys!(time, payload)
    payload['duration_s'] = time[:duration].round(6)
    payload['completed_at'] = Time.now.utc.to_f
  end

  def elapsed(t0)
    t1 = get_time
    { duration: t1[:now] - t0[:now] }
  end

  def get_time
    { now: current_time }
  end

  def current_time
    Process.clock_gettime(Process::CLOCK_MONOTONIC, :float_second)
  end

  def queue_duration_for_job(job)
    # Old gitlab-shell messages don't provide enqueued_at/created_at attributes
    enqueued_at = job['enqueued_at'] || job['created_at']
    return unless enqueued_at

    enqueued_at_time = convert_to_time(enqueued_at)
    return unless enqueued_at_time

    round_elapsed_time(enqueued_at_time)
  end

  def enqueue_latency_for_scheduled_job(job)
    scheduled_at = job['scheduled_at']
    enqueued_at = job['enqueued_at']

    return unless scheduled_at && enqueued_at

    scheduled_at_time = convert_to_time(scheduled_at)
    enqueued_at_time = convert_to_time(enqueued_at)

    return unless scheduled_at_time && enqueued_at_time

    round_elapsed_time(scheduled_at_time, enqueued_at_time)
  end

  def round_elapsed_time(start, end_time = Time.now)
    # It's possible that if there is clock-skew between two nodes this
    # value may be less than zero. In that event, we record the value
    # as zero.
    [elapsed_by_absolute_time(start, end_time), 0].max.round(6)
  end

  def elapsed_by_absolute_time(start, end_time)
    (end_time - start).to_f.round(6)
  end

  def convert_to_time(time_value)
    return time_value if time_value.is_a?(Time)
    return Time.iso8601(time_value) if time_value.is_a?(String)
    return Time.at(time_value) if time_value.is_a?(Numeric) && time_value > 0
  rescue ArgumentError
    # Swallow invalid dates. Better to loose some observability
    # than bring all background processing down because of a date
    # formatting bug in a client
  end
end
