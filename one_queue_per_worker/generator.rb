require 'sidekiq'
require 'active_support'
require_relative '../application_worker.rb'

puts "Starting sidekiq load generation"

Dir.glob("#{Dir.getwd}/workers/**/*.rb").each {|file| require file}

def schedule(type)
  (COUNTS[type.to_sym]*2).times do
    klass = $shard_probs[type.to_sym].bsearch do |p|
      p[1] > rand
    end.first
    klass.perform_async(1,2,3)
  end
end

#Number of workers for each shard, so we can scale our generation appropriately
COUNTS = {
  'catchall-k8s': 196*15,
  'catchall-vm': 56*15,
  'memory-bound': 16*1,
  'database-throttled': 1*5,
  'gitaly-throttled': 3*8,
  'elasticsearch': 8*2,
  'low-urgency-cpu-bound': 100*5,
  'urgent-cpu-bound': 84*5,
  'urgent-other': 130*5
}

# From raw data about counts of queues over a period (extracted from logs)
# construct a set of data that we can Array#bsearch over using rand
# This code is ugly.  Do not copy it.  I recommend against even attempting to read it
$shard_probs = {}
COUNTS.keys.each do |shard|
  queues = {}
  File.open("#{shard}-proportions.txt").each do |line|
    queue,count = line.split(" ")
    next unless queue && count
    class_name = queue.split(":").map(&:camelize).join("::")+"Worker"
    klass = class_name.constantize
    queues[klass] = count.to_i
  end
  total = queues.values.sum.to_f
  p_queues = queues.map { |k,v| [k,v/total.to_f]}.to_h
  fp_queues = []
  p_queues.each_with_object({:total => 0}) {|q, sum| sum[:total] += q[1]; fp_queues.push([q[0], sum[:total]]) }
  $shard_probs[shard] = fp_queues
end

count = 0
loop do
  puts "Round #{count} at #{Time.now.strftime('%H:%M:%S.%L')}"

  schedule('catchall-k8s')
  schedule('catchall-vm')
  schedule('database-throttled')
  schedule('elasticsearch')
  schedule('gitaly-throttled')
  schedule('low-urgency-cpu-bound')
  schedule('memory-bound')
  schedule('urgent-cpu-bound')
  schedule('urgent-other')

# sleep(0.05)
  count = count + 1
end
