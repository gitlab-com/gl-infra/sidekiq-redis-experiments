require 'sidekiq-reliable-fetch'
require 'active_support'
require_relative 'application_worker.rb'
require_relative 'scheduled_enq.rb'
require_relative 'structured_logger.rb'

Dir.glob("workers/**/*.rb").each {|file| require_relative file}

Sidekiq.configure_server do |config|
  config.options[:semi_reliable_fetch] = true
  config.options[:strict] = false

  config.options[:scheduled_enq] =
    if ENV.fetch('SCHEDULED_POLLERS', '1').to_f > rand
      ScheduledEnq
    else
      ScheduledEnq::Null
    end

  config.log_formatter = Sidekiq::Logger::Formatters::JSON.new
  config.options[:job_logger] = StructuredLogger

  Sidekiq::ReliableFetch.setup_reliable_fetch!(config)
end

# From https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/sidekiq_logging/structured_logger.rb
module Patch
  module SidekiqClient
    private

    # This is a copy of https://github.com/mperham/sidekiq/blob/v6.2.2/lib/sidekiq/client.rb#L187-L194
    # but using `conn.pipelined` instead of `conn.multi`. The multi call isn't needed here because in
    # the case of scheduled jobs, only one Redis call is made. For other jobs, we don't really need
    # the commands to be atomic.
    def raw_push(payloads)
      @redis_pool.with do |conn| # rubocop:disable Gitlab/ModuleWithInstanceVariables
        conn.pipelined do
          atomic_push(conn, payloads)
        end
      end
      true
    end
  end
end

Sidekiq::Client.prepend Patch::SidekiqClient
