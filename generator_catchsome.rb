require 'sidekiq'
require 'active_support'
require_relative 'application_worker.rb'

puts "Starting sidekiq load generation"

Dir.glob("workers/**/*.rb").each {|file| require_relative file}

def schedule(type, klasses)
  (COUNTS[type.to_sym]*2).times do
    klasses.sample.perform_async(1,2,3)
  end
end

CATCHSOME = [UpdateNamespaceStatistics::NamespacesScheduleAggregationWorker, WebHookWorker, ProjectImportScheduleWorker, RepositoryUpdateMirrorWorker, PipelineBackground::CiBuildTraceChunkFlushWorker, GitGarbageCollectWorker]

REMAINDER = [RemoteMirrorNotificationWorker, GcpCluster::ClustersApplicationsUninstallWorker, GcpCluster::ClusterInstallAppWorker, PipelineCreation::RunPipelineScheduleWorker, Geo::GeoRepositoryDestroyWorker, ElasticIndexingControlWorker, UploadChecksumWorker, HashedStorage::HashedStorageProjectMigrateWorker, GcpCluster::ClustersCleanupServiceAccountWorker, DeleteUserWorker, RepositoryUpdateRemoteMirrorWorker, GithubImporter::GithubImportImportLfsObjectWorker, UpdateNamespaceStatistics::NamespacesRootStatisticsWorker, ElasticIndexingControlWorker, RepositoryForkWorker, GithubImporter::GithubImportImportLfsObjectWorker, Epics::EpicsUpdateEpicsDatesWorker, GcpCluster::ClusterUpdateAppWorker, IncidentManagement::IncidentManagementPagerDutyProcessIncidentWorker, GithubImporter::GithubImportStageImportIssuesAndDiffNotesWorker, TodosDestroyer::TodosDestroyerConfidentialIssueWorker, Geo::GeoHashedStorageAttachmentsMigrationWorker, ObjectPool::ObjectPoolDestroyWorker, HashedStorage::HashedStorageMigratorWorker]

#Number of workers for each shard, so we can scale our generation appropriately
COUNTS = {
  'catchsome-k8s': 98*15,
  'catchall-k8s-remainder': 98*15,
  'catchall-vm': 56*15,
  'memory-bound': 16*1,
  'database-throttled': 1*5,
  'gitaly-throttled': 3*8,
  'elasticsearch': 8*2,
  'low-urgency-cpu-bound': 100*5,
  'urgent-cpu-bound': 84*5,
  'urgent-other': 130*5
}
count = 0
loop do
  puts "Round #{count} at #{Time.now.strftime('%H:%M:%S.%L')}" 

  schedule('catchsome-k8s', CATCHSOME)
  schedule('catchall-k8s-remainder', REMAINDER)
  schedule('catchall-vm', [GroupsUpdateStatisticsWorker])
  schedule('database-throttled', [BackgroundMigrationWorker])
  schedule('elasticsearch', [ElasticIndexerWorker])
  schedule('gitaly-throttled', [SnippetsUpdateRepositoryStorageWorker])
  schedule('low-urgency-cpu-bound', [NewEpicWorker])
  schedule('memory-bound', [ProjectExportWorker])
  schedule('urgent-cpu-bound', [MergeRequestResetApprovalsWorker])
  schedule('urgent-other', [UpdateHighestRoleWorker])

# sleep(0.05)
  count = count + 1
end
