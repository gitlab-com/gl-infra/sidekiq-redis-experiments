class ScheduledEnq
  LUA_ZPOPBYSCORE = <<~EOS
    local key, min, max = KEYS[1], ARGV[1], ARGV[2]
    local jobs = redis.call("zrangebyscore", key, min, max, "limit", 0, 1)
    if jobs[1] then
      redis.call("zrem", key, jobs[1])
      return jobs[1]
    end
  EOS

  LUA_ZPOPBYSCORE_SHA = Digest::SHA1.hexdigest(LUA_ZPOPBYSCORE)

  class Null
    def enqueue_jobs(*)
    end
  end

  def enqueue_jobs(now = Time.now.to_f.to_s, sorted_sets = Sidekiq::Scheduled::SETS)
    case ENV['SCHEDULED_ENQ']
    when 'atomic'
      atomic(now, sorted_sets)
    when 'sidekiq_5'
      sidekiq_5(now, sorted_sets)
    else
      sidekiq_6(now, sorted_sets)
    end
  end

  def atomic(_now, sorted_sets)
    Sidekiq.logger.info('Using atomic enqueuer')
    Sidekiq.redis do |conn|
      sorted_sets.each do |sorted_set|
        start_time = Process.clock_gettime(Process::CLOCK_MONOTONIC, :float_second)
        jobs = 0

        Sidekiq.logger.info(message: 'Enqueuing scheduled jobs', status: 'start', sorted_set: sorted_set)

        while job = redis_eval_lua(conn, LUA_ZPOPBYSCORE, LUA_ZPOPBYSCORE_SHA, keys: [sorted_set], argv: ['-inf', Time.now.to_f.to_s])
          jobs += 1
          Sidekiq::Client.push(Sidekiq.load_json(job))
        end

        end_time = Process.clock_gettime(Process::CLOCK_MONOTONIC, :float_second)
        Sidekiq.logger.info(message: 'Enqueuing scheduled jobs',
                            status: 'done',
                            sorted_set: sorted_set,
                            jobs_count: jobs,
                            duration_s: end_time - start_time)
      end
    end
  end

  def sidekiq_5(now, sorted_sets)
    Sidekiq.logger.info('Using sidekiq_5 enqueuer')
    # A job's "score" in Redis is the time at which it should be processed.
    # Just check Redis for the set of jobs with a timestamp before now.
    Sidekiq.redis do |conn|
      sorted_sets.each do |sorted_set|
        start_time = Process.clock_gettime(Process::CLOCK_MONOTONIC, :float_second)
        jobs = redundant_jobs = 0

        Sidekiq.logger.info(message: 'Enqueuing scheduled jobs', status: 'start', sorted_set: sorted_set)

        # Get the next item in the queue if it's score (time to execute) is <= now.
        # We need to go through the list one at a time to reduce the risk of something
        # going wrong between the time jobs are popped from the scheduled queue and when
        # they are pushed onto a work queue and losing the jobs.
        while (job = conn.zrangebyscore(sorted_set, "-inf", now, limit: [0, 1]).first)

          # Pop item off the queue and add it to the work queue. If the job can't be popped from
          # the queue, it's because another process already popped it so we can move on to the
          # next one.
          if conn.zrem(sorted_set, job)
            jobs += 1
            Sidekiq::Client.push(Sidekiq.load_json(job))
          else
            redundant_jobs += 1
          end
        end

        end_time = Process.clock_gettime(Process::CLOCK_MONOTONIC, :float_second)
        Sidekiq.logger.info(message: 'Enqueuing scheduled jobs',
                            status: 'done',
                            sorted_set: sorted_set,
                            jobs_count: jobs,
                            redundant_jobs_count: redundant_jobs,
                            duration_s: end_time - start_time)
      end
    end
  end

  def sidekiq_6(now, sorted_sets)
    Sidekiq.logger.info('Using sidekiq_6 enqueuer')
    # A job's "score" in Redis is the time at which it should be processed.
    # Just check Redis for the set of jobs with a timestamp before now.
    Sidekiq.redis do |conn|
      sorted_sets.each do |sorted_set|
        # Get next items in the queue with scores (time to execute) <= now.
        until (jobs = conn.zrangebyscore(sorted_set, "-inf", now, limit: [0, 100])).empty?
          # We need to go through the list one at a time to reduce the risk of something
          # going wrong between the time jobs are popped from the scheduled queue and when
          # they are pushed onto a work queue and losing the jobs.
          jobs.each do |job|
            # Pop item off the queue and add it to the work queue. If the job can't be popped from
            # the queue, it's because another process already popped it so we can move on to the
            # next one.
            if conn.zrem(sorted_set, job)
              Sidekiq::Client.push(Sidekiq.load_json(job))
              Sidekiq.logger.debug { "enqueued #{sorted_set}: #{job}" }
            end
          end
        end
      end
    end
  end

  def redis_eval_lua(conn, script, sha, keys: nil, argv: nil)
    conn.evalsha(sha, keys: keys, argv: argv)
  rescue ::Redis::CommandError => e
    if e.message.start_with?('NOSCRIPT')
      conn.eval(script, keys: keys, argv: argv)
    else
      raise
    end
  end
end
