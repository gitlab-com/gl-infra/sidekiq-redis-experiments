require 'yaml'
require 'fileutils'
require 'active_support/core_ext/string'

%w[app/workers/all_queues.yml ee/app/workers/all_queues.yml].each do |queues|
ws = YAML.load_file(queues)
ws.each do |w|
  class_path = w[:name].split(':').map {|s| s.camelize}
  class_name = class_path.last
  module_path = Array(class_path.dup)
  module_path.pop
  file_parts = w[:name].split(':')
  dirs = file_parts.dup
  dirs.pop
  FileUtils.mkdir_p("/tmp/workers/#{Array(dirs).join("/")}") if dirs.size > 0
  File.open("/tmp/workers/#{file_parts.join("/")}.rb", "w") do |f|
    classdef = <<-CLASSDEF
class #{class_name}Worker
  include ApplicationWorker

  def perform(*args)
    Sidekiq.redis { |r| r.ping } # hack to see job processing rate in redis stats
    sleep = self.class.job_duration 
    puts \"#{class_name} sleeping for \#{sleep}\"
    sleep(sleep)
  end
end
CLASSDEF
    module_path.reverse.each do |m|
     classdef = "module #{m}\n#{classdef}\nend"
    end
    f.write classdef
  end
end
end

