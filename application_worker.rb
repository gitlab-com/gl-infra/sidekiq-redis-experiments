# frozen_string_literal: true

require 'sidekiq/api'
require 'active_support/core_ext/string'

Sidekiq::Worker.extend ActiveSupport::Concern

module ApplicationWorker
  extend ActiveSupport::Concern

  include Sidekiq::Worker # rubocop:disable Cop/IncludeSidekiqWorker

  DECILES = [0.02, 0.028, 0.04, 0.054, 0.076, 0.115, 0.225, 0.389, 0.761]
  P95 = 1.374
  P99 = 3.647

  included do
    set_queue
  end

  class_methods do
    def set_queue
      queue_name = [queue_namespace, base_queue_name].compact.join(':')

      sidekiq_options queue: queue_name # rubocop:disable Cop/SidekiqOptionsQueue
    end

    def queue_namespace
      parts = name.split('::')
      parts.pop
      return nil unless parts.size > 0

      tparts = parts.map { |p| p.underscore.tr('/', '_') }
      tparts.join(':')
    end

    def base_queue_name
      name
        .split('::')
        .last
        .sub(/\AGitlab::/, '')
        .sub(/Worker\z/, '')
        .underscore
        .tr('/', '_')
    end

    def job_duration
      (([DECILES.first] * 14) + (DECILES[1..-1] * 10) + ([P95] * 5) + [P99]).sample
    end
  end
end
