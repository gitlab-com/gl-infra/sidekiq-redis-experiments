#!/bin/bash
PATH=$PATH:/opt/gitlab/embedded/bin/

if [ $# -ne 3 ]; then
  echo "Usage: $0 SCENARIO SHARDNAME PROCESSCOUNT"
  exit 1
fi

SCENARIO=$1
SHARD=$2
NUMBER=$3

for i in $(seq 1 $NUMBER); do
  bundle exec sidekiq -r ./sidekiq.rb -C ${SCENARIO}/${SHARD}.yml > logs/${SHARD}-${i}.log &
done

wait
