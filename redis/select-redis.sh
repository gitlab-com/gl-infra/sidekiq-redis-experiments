#!/bin/bash

if [ -z "$1" ] ; then
  echo "Usage: $0 [5|6]"
elif [ $1 = "6p" ]; then
  VER=6.0.10.patched
elif [ $1 -eq 5 ]; then
  VER=5.0.9
elif [ $1 -eq 6 ]; then
  VER=6.0.10
else
  echo "Unknown version"
  exit 1;
fi

alias redis-server="/opt/redis/redis-server-${VER} /opt/redis/redis.conf.${VER}"
alias redis-cli="/opt/redis/redis-cli-${VER}"
