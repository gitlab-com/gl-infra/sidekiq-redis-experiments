require 'sidekiq'
require 'active_support'
require_relative 'application_worker.rb'

total_queued_jobs = 0
Sidekiq::Queue.all.each do |queue|
  queue_size = queue.size
  total_queued_jobs += queue_size
  next if queue_size == 0
  puts "#{queue.name} #{queue.size}"
end
puts "Total #{total_queued_jobs}"
